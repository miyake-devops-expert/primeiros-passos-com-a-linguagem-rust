# Primeiros passos com a linguagem RUST

Repositorio para guardar os arquivos de exemplos e aprendizados do livro: Primeiros passos com a Linguagem Rust do autor José Augusto N. G. Manzano

Do capitulo 3 em diante foi somente um resumo com os pontos mais importantes. 

Não foram feitos os exercicíos, motivo: a criação de projetos práticos reais agrega mais para o meu aprendizado.