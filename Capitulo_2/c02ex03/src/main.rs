use std::io;
use std::io::prelude::*;

fn main() {
    let mut valor = String::new();
    let valor_convertido : i64;

    print!("Inform um valor inteiro: ");
    io::stdout().flush().unwrap();
    io::stdin().read_line(&mut valor).expect("Entrada Incorreta!");
    valor_convertido = valor.trim().parse::<i64>().unwrap();
    println!();

    println!("Resultado = {}\n", valor_convertido);
    print!("\nTecle <Enter> para encerrar...");
    io::stdout().flush().unwrap();
    io::stdin().read(&mut [0u8]).unwrap();
}