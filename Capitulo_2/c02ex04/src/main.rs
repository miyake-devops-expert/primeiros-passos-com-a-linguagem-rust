use std::io;
use std::io::prelude::*;

fn main() {
    let mut hora_trabalhada = String::new();
    let mut valor_hora = String::new();
    let mut percentual_de_desconto = String::new();

    let hora_trabalhada_convertido : f64;
    let valor_hora_convertido : f64;
    let percentual_de_desconto_convertido : f64;
    let total_descontado : f64;
    let salario_bruto : f64;
    let salario_liquido : f64;

    print!("Informe a quantidade de horas trabalhadas: ");
    io::stdout().flush().unwrap();
    io::stdin().read_line(&mut hora_trabalhada).expect("Entrada Incorreta!");
    hora_trabalhada_convertido = hora_trabalhada.trim().parse::<f64>().unwrap();
    println!();

    print!("Informe o valor da hora de trabalho: ");
    io::stdout().flush().unwrap();
    io::stdin().read_line(&mut valor_hora).expect("Entrada Incorreta!");
    valor_hora_convertido = valor_hora.trim().parse::<f64>().unwrap();
    println!();

    print!("Inform o valor do percentual de desconto: ");
    io::stdout().flush().unwrap();
    io::stdin().read_line(&mut percentual_de_desconto).expect("Entrada Incorreta!");
    percentual_de_desconto_convertido = percentual_de_desconto.trim().parse::<f64>().unwrap();
    println!();

    salario_bruto = hora_trabalhada_convertido * valor_hora_convertido;
    total_descontado = (percentual_de_desconto_convertido / 100.) * salario_bruto;
    salario_liquido = salario_bruto - total_descontado;

    println!();
    println!("Salario Bruto      = {:8.2}\n", salario_bruto);
    println!("Total de descontos = {:8.2}\n", total_descontado);
    println!("Salario Liquido    = {:8.2}\n", salario_liquido);

    println!();
    print!("\nTecle <Enter> para encerrar...");
    io::stdout().flush().unwrap();
    io::stdin().read(&mut [0u8]).unwrap();
}