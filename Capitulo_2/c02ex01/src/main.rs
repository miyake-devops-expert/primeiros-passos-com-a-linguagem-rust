use std::io;
use std::io::prelude::*;

fn main() {
    println!("Programação na Linguagem Rust,\n");
    print!("Tecle <enter> para encerrar...");
    io::stdout().flush().unwrap();
    io::stdin().read(&mut [0u8]).unwrap();

}
