use std::io;
use std::io::prelude::*;

trait CalcArea {
    fn area(&self) -> f32;
}


struct Circunferencia {
    raio: f32,
} impl CalcArea for Circunferencia{
    fn area(&self) -> f32 {
        return self.raio.powi(2) * std::f32::consts::PI
    }

}

struct Quadrado {
    lado: f32,
} impl CalcArea for Quadrado {
    fn area(&self) -> f32 {
        return self.lado.powi(2)
    }
}

fn calc_area(figura: &dyn CalcArea) {
    println!("Área calculada = {}", figura.area())
}

fn pausa() {
    println!();
    print!("Tecle <Enter> para Encerrar... ");
    io::stdout().flush().unwrap();
    io::stdin().read(&mut [0u8]).unwrap();
}

fn main() {
    let mut entra_lado = String::new();
    let mut entra_raio = String::new();

    let raio_ent: f32;
    let lado_ent: f32;

    print!("Entre com um valor do lado de um quadrado.. : ");
    io::stdout().flush().unwrap();
    io::stdin().read_line(&mut entra_lado).unwrap();
    lado_ent = entra_lado.trim().parse::<f32>().unwrap();
    calc_area(&Quadrado{lado: lado_ent});

    println!();
    print!("Entre com um valor do raio da circunferencia.. : ");
    io::stdout().flush().unwrap();
    io::stdin().read_line(&mut entra_raio).unwrap();
    raio_ent = entra_raio.trim().parse::<f32>().unwrap();
    calc_area(&Circunferencia{raio: raio_ent});


    pausa();

}