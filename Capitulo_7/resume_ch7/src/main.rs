use std::io;
use std::io::prelude::*;


fn pausa() {
    println!();
    print!("Tecle <Enter> para Encerrar... ");
    io::stdout().flush().unwrap();
    io::stdin().read(&mut [0u8]).unwrap();
}

fn main() {

    let valores1: [i32; 5] = [1,2,3,4,5];
    let valores2: [i32; 5] = [6,7,8,9,0];
    let valores3: [i32; 5] = [81,42,33,13,5];

    let lista = valores1.iter();
    println!("Produto da Matriz .............: {:4}", lista.product::<i32>());
    println!();
    let lista = valores1.iter();
    println!("QTD de elemetos da Matriz .............: {:4}", lista.count());

    println!();
    let lista = valores1.iter();
    println!("Ultimo elemento da Matriz .............: {:?}", lista.last());
    println!();
    let mut  lista = valores1.iter();
    println!("Terceiro elemento da Matriz .............: {:?}", lista.nth(2));
    println!();

    let mut  lista = valores1.iter().chain(valores2.iter()).skip(5);
    for i in 0 .. 6 {
        println!("Elemento {:2} .............: {:?}", i, lista.next());
    }
    println!();
    let mut lista = valores1.iter().zip(valores2.iter());
    for i in 0 .. 5 {
        println!("Elemento {:2} .............: {:?}", i, lista.next());
    }
    println!();
    let lista = valores3.iter();
    println!("Menor elemento da Matriz .............: {:?}", lista.min());
    println!();
    let lista = valores3.iter();
    println!("Maior elemento da Matriz .............: {:?}", lista.max());
    println!();

    pausa();

}