use std::io;
use std::io::prelude::*;


fn pausa() {
    println!();
    print!("Tecle <Enter> para Encerrar... ");
    io::stdout().flush().unwrap();
    io::stdin().read(&mut [0u8]).unwrap();
}

fn main() {
    let  valor = &10i32;
    print!("Entre com um valor a ser realizado o fatorial... : ");
    match valor {
        &val => println!("Valor por desestruturação (&) ..: {:?}", val)
    }
    match *valor {
        val => println!("Valor por desreferência (*) ..: {:?}", val)
    }
    match valor {
        ref val => println!("Valor por referência ..: {:?}", val)
    }
    pausa();
}